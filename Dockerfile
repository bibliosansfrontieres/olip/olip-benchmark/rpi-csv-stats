########
# BASE #
########

FROM node:21-alpine3.19 AS base

# Pnpm configuration
ENV PNPM_HOME="/pnpm"
ENV PATH="$PNPM_HOME:$PATH"
RUN corepack enable

WORKDIR /app

COPY package*.json pnpm-lock.yaml ./

RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm install --prod --frozen-lockfile

#########
# BUILD #
#########

FROM base AS build

RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm install --frozen-lockfile

COPY . .

RUN pnpm run build

##############
# PRODUCTION #
##############

FROM base AS production

COPY --from=build /app/dist /app/dist
COPY --from=build /app/logs /app/logs

CMD ["node", "dist/index.js"]