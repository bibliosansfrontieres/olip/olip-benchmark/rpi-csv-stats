import { FileWriter } from './file-writer'
import dotenv from 'dotenv'
import { SystemInformations } from './interfaces/system-informations.interface'
import { getMemoryLoad } from './utils/memory/get-memory-load.util'
import { getCpuTemperature } from './utils/cpu/get-cpu-temperature.util'
import { getCpuFrequency } from './utils/cpu/get-cpu-frequency.util'
import { getCpuLoad } from './utils/cpu/get-cpu-load.util'
dotenv.config()

export class StatsChecker {
  private fileWriter: FileWriter

  constructor() {
    this.fileWriter = new FileWriter()
  }

  run() {
    setInterval(
      this.checkInformations.bind(this),
      parseInt(process.env.INTERVAL || '2000')
    )
    console.log(`Stats checker started with ${process.env.INTERVAL || '2000'}ms interval`)
  }

  async checkInformations() {
    const informations = await this.getInformations()
    this.fileWriter.writeInformations(informations)
  }

  async getInformations(): Promise<SystemInformations> {
    const memoryLoad = await getMemoryLoad()
    const cpuTemperature = await getCpuTemperature()
    const cpuFrequency = await getCpuFrequency()
    const cpuLoad = await getCpuLoad()
    const informations: SystemInformations = {
      cpuTemperature: Math.ceil(cpuTemperature),
      cpuFrequency: cpuFrequency,
      cpuLoad: Math.ceil(cpuLoad),
      memoryLoad: Math.ceil(memoryLoad)
    }
    return informations
  }
}
