import { appendFileSync, existsSync, writeFileSync } from 'fs'
import { join } from 'path'
import { formatTimestamp } from './utils/format-timestamp.util'
import { SystemInformations } from './interfaces/system-informations.interface'

export class FileWriter {
  private filePath: string

  constructor() {
    const filename = `rpi-${Date.now()}.csv`
    this.filePath = join(__dirname, '../logs', filename)
    if(!existsSync(this.filePath)) {
      writeFileSync(
        this.filePath,
        'Date,CPU T°,CPU Freq GHz,CPU Load %,Memory load %\n'
      )
    }
    console.log(`Write informations in ${this.filePath}`)
  }

  writeInformations(infos: SystemInformations) {
    let csvLine = `${formatTimestamp(Date.now())}`
    csvLine += `,${infos.cpuTemperature}`
    csvLine += `,${infos.cpuFrequency}`
    csvLine += `,${infos.cpuLoad}`
    csvLine += `,${infos.memoryLoad}`
    appendFileSync(this.filePath, `${csvLine}\n`)
  }
}
