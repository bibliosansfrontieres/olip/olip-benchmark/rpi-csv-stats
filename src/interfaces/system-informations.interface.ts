export interface SystemInformations {
  cpuTemperature: number
  cpuFrequency: number
  cpuLoad: number
  memoryLoad: number
}
