import { Systeminformation, cpuCurrentSpeed } from 'systeminformation'

/**
 * Retrieves the average CPU frequency.
 *
 * @return {number} The average CPU frequency in GHz.
 */
export async function getCpuFrequency(): Promise<number> {
  const cpuCurrentSpeedData: Systeminformation.CpuCurrentSpeedData =
    await cpuCurrentSpeed()
  return cpuCurrentSpeedData.avg ?? 0
}
