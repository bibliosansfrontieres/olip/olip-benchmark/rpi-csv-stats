import { Systeminformation, currentLoad } from 'systeminformation'

/**
 * Retrieves the current CPU load.
 *
 * @return {number} The current CPU load as a percentage.
 */
export async function getCpuLoad(): Promise<number> {
  const currentLoadData: Systeminformation.CurrentLoadData = await currentLoad()
  return currentLoadData.currentLoad ?? 0
}
