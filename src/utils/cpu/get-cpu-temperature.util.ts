import { Systeminformation, cpuTemperature } from 'systeminformation'

/**
 * Retrieves the CPU temperature.
 *
 * @return {Promise<number>} The main CPU temperature in degrees Celsius.
 */
export async function getCpuTemperature(): Promise<number> {
  const cpuTemperatureData: Systeminformation.CpuTemperatureData =
    await cpuTemperature()
  return cpuTemperatureData.main ?? 0
}
