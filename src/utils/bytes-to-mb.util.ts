/**
 * Converts bytes to megabytes.
 *
 * @param {number} bytes - The number of bytes to convert.
 * @return {number} The equivalent number of megabytes.
 */
export function bytesToMb(bytes: number): number {
  return bytes / (1024 * 1024)
}
