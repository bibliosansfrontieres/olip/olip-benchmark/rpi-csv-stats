import { Systeminformation, mem } from 'systeminformation'

/**
 * Retrieves the memory load as a percentage.
 *
 * @return {Promise<number>} The memory load as a percentage.
 */
export async function getMemoryLoad(): Promise<number> {
  try {
    const memData: Systeminformation.MemData = await mem()
    const usagePurcentage = (memData.used * 100) / memData.total
    return usagePurcentage
  } catch {
    return 0
  }
}
