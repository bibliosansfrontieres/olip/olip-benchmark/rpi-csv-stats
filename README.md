# RPI CSV Stats

This project is used to get CPU temperature, CPU load
and Memory usage on a Raspberry Pi server.

## Usage

Ideascube devices [ship with a script](https://gitlab.com/bibliosansfrontieres/olip/olip-deploy/-/blob/master/roles/olip/files/run-rpi-csv-stats.sh)
which basically runs the container as such:

```shell
balena-engine run \
    -e "INTERVALL=${INTERVAL:-1000}" \
    -v /data/logs/rpi-csv-stats:/logs \
    bibliosansfrontieres/rpi-csv-stats:latest
```

This allows for `INTERVAL` to be configured using an environment variable.

## Contributing

A `docker-compose.yml` file is provided for development.

## This image

- GitLab:
  [bibliosansfrontieres/olip-ng/olip-benchmark/rpi-csv-stats](https://gitlab.com/bibliosansfrontieres/olip-ng/olip-benchmark/rpi-csv-stats)
- Docker Hub:
  [bibliosansfrontieres/rpi-csv-stats](https://hub.docker.com/r/bibliosansfrontieres/rpi-csv-stats)
  (manual builds for `amd64` & `arm64`)
